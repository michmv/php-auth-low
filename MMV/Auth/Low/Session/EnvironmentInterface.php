<?php

namespace MMV\Auth\Low\Session;

interface EnvironmentInterface
{
    public function setCookie(string $name, string $value = null, $expire = 0, ?string $path = '/', string $domain = null, bool $secure = null, bool $httpOnly = true);

    public function getCookie(string $name, string $default='');
}
