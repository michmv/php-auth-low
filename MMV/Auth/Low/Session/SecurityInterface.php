<?php

namespace MMV\Auth\Low\Session;

interface SecurityInterface
{
    public function encrypt(string $str): string;

    public function decrypt(string $str): string;

    public function uuid(): string;
}
