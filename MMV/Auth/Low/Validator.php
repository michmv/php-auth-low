<?php

namespace MMV\Auth\Low;

use DateTime;
use MMV\Auth\Low\Validator\ValidatorInterface;
use MMV\Auth\Low\StorageInterface;

class Validator implements ValidatorInterface
{
    /**
     * @var array
     */
    protected $messages = [];

    protected StorageInterface $storage;

    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * If return False to get messages `getMessages`.
     *
     * @param array $data
     * @param array $rules
     * @return bool
     */
    public function validate(array $data, array $rules): bool
    {
        $this->messages = [];

        $check_data = array_fill_keys(array_keys($data), ['rules_not_found']);

        foreach($rules as $name => $rule) {
            $t = $this->_checkRule($name, ($data[$name] ?? ''), $rule, $data);
            if($t !== true) $this->addMessages($name, $t);
            unset($check_data[$name]);
        }

        if(count($check_data))
            $this->messages = array_merge($check_data, $this->messages);

        return ! (bool)count($this->messages);
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /******************************************************************************/

    /**
     * @param string $key
     * @param array $messages
     */
    protected function addMessages($key, $messages)
    {
        if(array_key_exists($key, $this->messages)) {
            $this->messages[$key] =
                array_merge($this->messages[$key], $messages);
        } else {
            $this->messages[$key] = $messages;
        }
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param string $rules
     * @param array $data
     * @return true|string[]
     */
    protected function _checkRule($name, $value, $rules, $data)
    {
        $_rules = $this->_parseRules($rules);
        $result = [];

        foreach($_rules as $rule) {
            if($rule[0] == 'bail') {
                if(count($result)) return $result;
                else continue;
            }

            $method = '_valid'.$rule[0];
            $parameters = array_slice($rule, 1);
            $t = $this->$method($name, $value, $parameters, $data);
            if($t !== true) $result[] = $t;
        }

        return count($result) ? $result : true ;
    }

    /**
     * @param string $rules
     * @return array
     */
    protected function _parseRules($rules)
    {
        $_rules = explode('|', $rules);

        return array_map(function($v){
                return explode(':', $v);
            }, $_rules);
    }

    /******************************************************************************/

    /**
     * @param string $name
     * @param mixed $value
     * @param array $parameters
     * @param array $data
     * @return true|string
     */
    protected function _validRequired($name, $value, $parameters, $data)
    {
        if(is_array($value)) return 'value_must_not_be_array';

        if((string)$value === '') {
            return 'value_is_required';
        }
        return true;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param array $parameters
     * @param array $data
     * @return true|string
     */
    protected function _validLengthMax($name, $value, $parameters, $data)
    {
        if(is_array($value)) return 'value_must_not_be_array';

        if((string)$value === '') return true;

        if(mb_strlen($value) > $parameters[0]) {
            return 'string_max|'.$parameters[0];
        }
        return true;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param array $parameters
     * @param array $data
     * @return true|string
     */
    protected function _validLengthMin($name, $value, $parameters, $data)
    {
        if(is_array($value)) return 'value_must_not_be_array';

        if((string)$value === '') return true;

        if(mb_strlen($value) < $parameters[0]) {
            return 'string_min|'.$parameters[0];
        }
        return true;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param array $parameters
     * @param array $data
     * @return true|string
     */
    protected function _validEmail($name, $value, $parameters, $data)
    {
        if(is_array($value)) return 'value_must_not_be_array';

        if((string)$value === '') return true;

        if(filter_var($value, FILTER_VALIDATE_EMAIL) === false) {
            return 'incorrect_email_format';
        }
        return true;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param array $parameters
     * @param array $data
     * @return true|string
     */
    protected function _validAlphanumeric($name, $value, $parameters, $data)
    {
        if(is_array($value)) return 'value_must_not_be_array';

        if((string)$value === '') return true;

        $pattern = '/^[a-zA-Z][a-zA-Z0-9-_\.]*$/';
        if(!preg_match($pattern, $value)) {
            return 'incorrect_alphanumeric';
        }

        $pattern = '/[-_.][-_.]/';
        if(preg_match($pattern, $value)) {
            return 'double_separation_symbol';
        }

        return true;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param array $parameters
     * @param array $data
     * @return true|string
     */
    protected function _validBoolInt($name, $value, $parameters, $data)
    {
        if(is_array($value)) return 'value_must_not_be_array';

        if((string)$value === '') return true;

        if((string)$value === '1' || (string)$value == '0') {
            return true;
        }
        return 'incorrect_bool_int';
    }
    
    /**
     * @param string $name
     * @param mixed $value
     * @param array $parameters
     * @param array $data
     * @return true|string
     */
    protected function _validPositiveInt($name, $value, $parameters, $data)
    {
        if(is_array($value)) return 'value_must_not_be_array';

        if((string)$value === '') return true;

        $pattern = '/^[0-9]+$/';
        if(preg_match($pattern, $value) && (int)$value >= 0) {
            return true;
        }

        return 'value_not_positive_int';
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param array $parameters
     * @param array $data
     * @return true|string
     */
    protected function _validDate($name, $value, $parameters, $data)
    {
        if(is_array($value)) return 'value_must_not_be_array';

        if((string)$value === '') return true;

        $pattern = '/^[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2} [0-9]{2,2}:[0-9]{2,2}:[0-9]{2,2}$/';
        if(!preg_match($pattern, $value)) {
            return 'incorrect_date_format';
        }

        try {
            $date = new DateTime($value);
        } catch (\Exception $e) {
            return 'date_invalid';
        }

        return true;
    }

    /**
     * Example: unique:table:field
     * 
     * @param string $name
     * @param mixed $value
     * @param array $parameters
     * @param array $data
     * @return true|string
     */
    protected function _validUnique($name, $value, $parameters, $data)
    {
        if(is_array($value)) return 'value_must_not_be_array';

        if((string)$value === '') return true;

        $count = $this->storage->countRecord([[$parameters[1], '=', $value]], $parameters[0]);

        if($count > 0) {
            return 'not_unique_record';
        }

        return true;
    }
    
    /**
     * Example: uniqueExcludeThis:nameExcludeParameter:table:field
     * 
     * @param string $name
     * @param mixed $value
     * @param array $parameters
     * @param array $data
     * @return true|string
     */
    protected function _validUniqueExcludeThis($name, $value, $parameters, $data)
    {
        if(is_array($value)) return 'value_must_not_be_array';

        if((string)$value === '') return true;

        // uniqueExcludeThis:id:table:name
        $exclude = (string)$parameters[0];    // field for exclude
        if(array_key_exists($exclude, $data)) {
            $exclude_value = $data[$exclude]; // value for exclude
        } else {
            throw new \RuntimeException(sprintf('Not found %s in data array', $exclude));
        }
        $table = (string)$parameters[1];      // table
        $field = (string)$parameters[2];      // field

        $count = $this->storage->countRecord(
            [ [$field, '=', $value], [$exclude, '<>', $exclude_value] ],
            $table
        );

        if($count > 0) {
            return 'not_unique_record_exclude_this';
        }

        return true;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param array $parameters
     * @param array $data
     * @return true|string
     */
    protected function _validSafe($name, $value, $parameters, $data)
    {
        return true;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param array $parameters
     * @param array $data
     * @return true|string
     */
    protected function _validConfirmPassword($name, $value, $parameters, $data)
    {
        if(is_array($value)) return 'value_must_not_be_array';

        if((string)$value === '') return true;

        if($data[$parameters[0]] === (string)$value) return true;

        return 'not_confirm_password';
    }

    protected function _validInArray($name, $value, $parameters, $data)
    {
        if(is_array($value)) return 'value_must_not_be_array';

        if((string)$value === '') return true;

        foreach($parameters as $it) {
            if((string)$value === (string)$it) return true;
        }

        return 'not_exists_in_array';
    }
}
