<?php

namespace MMV\Auth\Low;

use MMV\Auth\Low\User;
use MMV\Auth\Low\Validator\ValidatorInterface;
use MMV\Auth\Low\Auth\SecurityInterface;
use MMV\Auth\Low\Auth\SessionInterface;
use MMV\Auth\Low\StorageInterface;
use MMV\Auth\Low\Auth\EnvironmentInterface;
use MMV\Auth\Low\Access;

class Auth
{
    public $tables = [
        'users'          => 'auth_users',
        'email_confirm'  => 'auth_email_confirm',
        'password_reset' => 'auth_password_reset',
        'failed_signin'  => 'auth_failed_signin',
    ];

    public int $emailConfirmTimeLife = 3600; // 60 * 60 * 1

    public int $passwordResetTimeLife = 3600; // 60 * 60 * 1

    public int $timeBlockSignIn = 600; // 60 * 10

    public int $limitTrialsSignin = 10;

    public int $sessionShortTimeLife = 86400; // 60 * 60 * 24

    public int $sessionLongTimeLife = 1209600; // 60 * 60 * 24 * 14

    public EnvironmentInterface $environment;

    protected ?Access $access = null;

    protected SecurityInterface $security;

    protected SessionInterface $session;

    protected ValidatorInterface $validator;

    protected StorageInterface $storage;

    protected ?User $user = null;

    protected bool $userInit = false;

    protected $messages = [];

    public function __construct(ValidatorInterface $validator, StorageInterface $storage,
        SecurityInterface $security, SessionInterface $session, EnvironmentInterface $environment,
        ?Access $access=null)
    {
        $this->validator = $validator;
        $this->storage = $storage;
        $this->security = $security;
        $this->session = $session;
        $this->environment = $environment;
        $this->access = $access;
    }

    /**
     * @param string|string[] $resource
     * @return boolean
     */
    public function check($resource): bool
    {
        if(is_null($this->access) || $this->isGuest()) return false;

        if(is_string($resource)) $resource = [$resource];

        foreach($resource as $it) {
            if(!$this->access->checkResource((int)$this->user()->role, $it))
                return false;
        }

        return true;
    }

    /**
     * Registration new user
     * If this method return Null to call getMessages for read messages
     */
    public function signUp(array $data): ?User
    {
        $_user = $this->makeUser($data);

        $rules = $this->rulesSignUp($_user->getRulesInsert());
        if(!$this->validator->validate($data, $rules)) {
            $this->messages = $this->validator->getMessages();
            return null;
        }

        $_user->password = $this->security->hash($_user->password);

        $res = $_user->save();
        if($res) {
            return $_user;
        }
        else {
            $this->messages = $_user->getMessages();
            return null;
        }
    }

    public function signIn(string $email, string $password): ?User
    {
        // check data
        $_user = $this->makeUser([]);
        $rules = $this->rulesSignIn($_user->getRulesInsert());
        if(!$this->validator->validate(['email'=>$email, 'password'=>$password], $rules)) {
            $this->messages = $this->validator->getMessages();
            return null;
        }

        // find user
        $user = $this->storage->findRecord([['email', '=', $email]], $this->tables['users']);
        if(!$user) {
            $this->addMessage('form', 'email_or_password_incorrect');
            return null;
        }
        $user = $user[0];

        // check failed signin
        $failed = $this->findFailedSignIn($user);
        if($failed && $failed->count >= $this->limitTrialsSignin) {
            $this->addMessage('form', 'signin_was_blocked');
            return null;
        }

        // check password
        if(!$this->security->check($password, $user->password)) {
            $this->incrementFailed($failed, $user);
            $this->addMessage('form', 'password_not_verification');
            return null;
        }

        // ok
        $this->user = $this->makeUser((array)$user);
        $this->userInit = true;
        return $this->user;
    }

    public function changePasswordForUser(User $user, $newPassword, $confirmPassword, int $recordId=0, $confirmedEmail=true): bool
    {
        // valid data
        $_user = $this->makeUser([]);
        $rules = $this->rulesChangePassword($_user->getRulesInsert());
        if(!$this->validator->validate(['password'=>$newPassword, 'confirm'=>$confirmPassword], $rules)) {
            $this->messages = $this->validator->getMessages();
            return false;
        }

        $this->beginTransaction();

        $user->password = $this->security->hash($newPassword);
        if($confirmedEmail) $user->email_confirmed = 1;
        $user->updated_at = time();
        if(!$user->save()) {
            $this->rollbackTransaction();
            $this->messages = $user->getMessages();
            return false;
        }

        $this->storage->deleteRecord([['record_id', '=', $recordId]], $this->tables['password_reset']);

        $this->session->removeAllSessionForUser($user->id);

        $this->commitTransaction();

        return true;
    }

    public function signOut()
    {
        if(!$this->isGuest()) {
            $this->session->unlinkUserId();
            $this->user = null;
            $this->userInit = true;
        }
    }

    public function signOUtAll(User $user)
    {
        if(!$this->isGuest()) {
            $this->session->removeAllSessionForUser($user->id);
            $this->user = null;
            $this->userInit = true;
        }
    }

    public function closeAllOtherSession()
    {
        if(!$this->isGuest())
            $this->session->deleteOtherSessionOfUser();
    }

    public function createSession(User $user, bool $rememberMe): bool
    {
        $this->session->setUserId($user->id);

        $duration = $rememberMe ? $this->sessionLongTimeLife : $this->sessionShortTimeLife;
        $this->session->changeDuration($duration);

        return true;
    }

    public function createEmailConfirm(User $user)
    {
        $code = $this->security->randomString(48);

        $data = [
            'user_id'=>$user->id,
            'code'=>$this->security->hash($code),
            'time_life' => time() + $this->emailConfirmTimeLife,
        ];

        $recordId = $this->storage->insertRecord($data, $this->tables['email_confirm']);

        return [$recordId, $code];
    }

    /**
     * @param User $user
     * @return array [recordId, code]
     */
    public function createResetPassword(User $user): array
    {
        $code = $this->security->randomString(48);

        $data = [
            'user_id' => $user->id,
            'code' => $this->security->hash($code),
            'time_life' => time() + $this->passwordResetTimeLife,
        ];

        $recordId = $this->storage->insertRecord($data, $this->tables['password_reset']);

        return [$recordId, $code];
    }

    /**
     * @param string|int $recordId
     * @param string $code
     * @return User|null
     */
    public function findUserByRecordResetPassword($recordId, $code): ?User
    {
        $record = $this->storage->findRecord([['record_id', '=', $recordId], ['time_life', '>', time()]], $this->tables['password_reset']);
        if($record && $this->security->check($code, $record[0]->code)) {
            $user = $this->storage->findRecord([['id', '=', $record[0]->user_id]], $this->tables['users']);
            if($user) {
                return $this->makeUser((array)$user[0]);
            }
        }
        return null;
    }

    public function confirmEmail($recordId, $code): ?User
    {
        $record = $this->storage->findRecord([['record_id', '=', $recordId], ['time_life', '>', time()]], $this->tables['email_confirm']);
        if($record) {
            $record = $record[0];

            if($this->security->check($code, $record->code)) {

                $user = $this->storage->findRecord([['id', '=', $record->user_id]], $this->tables['users']);
                if($user) {
                    $this->storage->beginTransaction();

                    // update user
                    $user = $this->makeUser((array)$user[0]);
                    $user->email_confirmed = 1;
                    $user->updated_at = time();
                    $user->save();

                    // delete record
                    $this->storage->deleteRecord([['record_id', '=', $recordId]], $this->tables['email_confirm']);

                    $this->commitTransaction();

                    return $user;
                }
            }
        }
        return null;
    }

    public function findUserByEmail(string $email): ?User
    {
        // check data
        $_user = $this->makeUser([]);
        $rules = $this->rulesForgotPassword($_user->getRulesInsert());
        if(!$this->validator->validate(['email'=>$email], $rules)) {
            $this->messages = $this->validator->getMessages();
            return null;
        }

        // find user
        $user = $this->storage->findRecord([['email', '=', $email]], $this->tables['users']);
        if(!$user) {
            $this->addMessage('email', 'user_not_found');
            return null;
        }

        return $this->makeUser((array)$user[0]);
    }

    /**
     * @param string|int $user_id
     * @return User|null
     */
    public function findUserById($user_id): ?User
    {
        $user = $this->storage->findRecord([['id', '=', $user_id]], $this->tables['users']);
        if(!$user) return null;
        else return $this->makeUser((array)$user[0]);
    }

    public function isGuest(): bool
    {
        return !((bool)$this->user());
    }

    public function user(): ?User
    {
        if(!$this->userInit) {
            $userId = $this->session->getUserId();
            if($userId) {
                $user = $this->storage->findRecord([['id', '=', $userId]], $this->tables['users']);
                if($user) {
                    $user = $user[0];
                    $this->user = $this->makeUser((array)$user);
                }
            }
            $this->userInit = true;
        }
        return $this->user;
    }

    public function makeUser(array $data=[]): User
    {
        $user = new User($this->validator, $this->storage, $this->access);
        $user->tableName = $this->tables['users'];
        foreach($user->listFiled() as $field) {
            if(array_key_exists($field, $data))
                $user->$field = $data[$field];
        }
        return $user;
    }

    public function beginTransaction()
    {
        $this->storage->beginTransaction();
    }

    public function commitTransaction()
    {
        $this->storage->commitTransaction();
    }

    public function rollbackTransaction()
    {
        $this->storage->rollbackTransaction();
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function addMessage(string $field, string $message)
    {
        $this->messages[$field][] = $message;
    }

    public function clearNoConfirmUser(int $now, int $storageLife)
    {
        $this->storage->deleteRecord([
            ['email_confirmed', '=', 0],
            ['created_at', '<', $now - $storageLife],
        ], $this->tables['users']);
    }

    public function clearEmailConfirm(int $now)
    {
        $this->storage->deleteRecord([['time_life', '<', $now]], $this->tables['email_confirm']);
    }

    public function clearPasswordReset(int $now)
    {
        $this->storage->deleteRecord([['time_life', '<', $now]], $this->tables['password_reset']);
    }

    public function clearFailedSignin(int $now)
    {
        $this->storage->deleteRecord([
            ['created_at', '<', $now - $this->timeBlockSignIn]
        ], $this->tables['failed_signin']);
    }

    protected function rulesSignUp(array $rules)
    {
        if(false !== $n = strpos($rules['email'], '|bail|'))
            $rules['email'] = substr($rules['email'], 0, $n);

        if(false !== $n = strpos($rules['login'], '|bail|'))
            $rules['login'] = substr($rules['login'], 0, $n);

        $res = [
            'email' => $rules['email'],
            'login' => $rules['login'],
            'password' => $rules['password'],
            'confirm' => 'required|confirmPassword:password',
        ];
        return $res;
    }

    protected function rulesChangePassword(array $rules)
    {
        $res = [
            'password' => $rules['password'],
            'confirm' => 'required|confirmPassword:password',
        ];
        return $res;
    }

    protected function rulesSignIn(array $rules)
    {
        if(false !== $n = strpos($rules['email'], '|bail|'))
            $rules['email'] = substr($rules['email'], 0, $n);
        
        return [
            'email' => $rules['email'],
            'password' => $rules['password'],
        ];
    }

    protected function rulesForgotPassword(array $rules)
    {
        if(false !== $n = strpos($rules['email'], '|bail|'))
            $rules['email'] = substr($rules['email'], 0, $n);
        
        return [
            'email' => $rules['email'],
        ];
    }

    protected function getAgentUser()
    {
        return $this->environment->getAgentId();
    }

    protected function findFailedSignIn(object $user): ?object
    {
        $failed = $this->storage->findRecord(
            [
                ['user_id', '=', $user->id],
                ['agent', '=', $this->getAgentUser()],
                ['created_at', '>', time() - $this->timeBlockSignIn]
            ],
            $this->tables['failed_signin']);

        return ($failed) ? $failed[0] : null;
    }

    /**
     * @param object $user
     * @param object|null $failed
     */
    protected function incrementFailed($failed, $user)
    {
        if($failed) {
            $this->storage->updateRecord(
                [ ['id', '=', $failed->id] ],
                ['count' => $failed->count + 1],
                $this->tables['failed_signin']
            );
        }
        else {
            $this->storage->insertRecord([
                'user_id' => $user->id,
                'agent' => $this->getAgentUser(),
                'count' => 1,
                'created_at' => time(),
            ], $this->tables['failed_signin']);
        }
    }
}
