<?php

namespace MMV\Auth\Low;

use MMV\Auth\Low\Validator\ValidatorInterface;
use MMV\Auth\Low\StorageInterface;
use MMV\Auth\Low\Access;

class User
{
    public string $tableName = 'users';

    /**
     * @var string|int
     */
    public $id;

    public string $email = '';

    public string $password = '';

    public string $login = '';

    public int $email_confirmed = 0;

    public int $created_at = 0;

    public int $updated_at = 0;

    public int $role = 0;
    
    protected ?Access $access = null;

    protected ValidatorInterface $validator;

    protected StorageInterface $storage;

    protected array $errors = [];

    public function __construct(ValidatorInterface $validator, StorageInterface $storage, ?Access $access=null)
    {
        $this->validator = $validator;
        $this->storage = $storage;
        $this->access = $access;
    }

    /**
     * @param string|string[] $resource
     * @return boolean
     */
    public function check($resource): bool
    {
        if(is_null($this->access)) return false;

        if(is_string($resource)) $resource = [$resource];

        foreach($resource as $it) {
            if(!$this->access->checkResource((int)$this->role, $it))
                return false;
        }

        return true;
    }

    /**
     * If return false to get message `getMessages`
     *
     * @param boolean $fake
     * @return boolean
     */
    public function save(bool $fake=false): bool
    {
        $rules = (!$this->id) ? $this->getRulesInsert() : $this->getRulesUpdate();
        $data = $this->toArray();
        if($this->validator->validate($data, $rules)) {

            if($fake) return true;

            if(!$this->id) {
                $this->id = $this->storage->insertRecord(
                    $this->preInsertUser($data), $this->tableName);
            }
            else {
                $this->storage->updateRecord(
                    [['id', '=', $this->id]], $this->preUpdateUser($data), $this->tableName);
            }

            return true;
        }

        return false;
    }

    public function delete(): bool
    {
        if($this->id) {
            $this->storage->deleteRecord([['id', '=', $this->id]], $this->tableName);
        }
        $this->id = '';
        return true;
    }

    /**
     * Get messages if was error in `save()` method
     */
    public function getMessages(): array
    {
        return $this->validator->getMessages();
    }

    public function listFiled()
    {
        return [ 'id', 'email', 'password', 'login', 'email_confirmed', 'created_at',
            'updated_at', 'role'];
    }

    public function getRulesInsert(): array
    {
        return [
            'id'              => 'safe',
            'email'           => 'required|email|lengthMax:64|bail|unique:'.$this->tableName.':email',
            'password'        => 'required|lengthMin:6|lengthMax:72', // must be as hash
            'login'           => 'required|alphanumeric|lengthMin:3|lengthMax:48|bail|unique:'.$this->tableName.':login',
            'email_confirmed' => 'required|inArray:0:1',
            'created_at'      => 'safe',
            'updated_at'      => 'safe',
            'role'            => 'required|positiveInt',
        ];
    }

    public function getRulesUpdate(): array
    {
        $rules = $this->getRulesInsert();
        $rules['email'] = str_replace('unique', 'uniqueExcludeThis:id', $rules['email']);
        $rules['login'] = str_replace('unique', 'uniqueExcludeThis:id', $rules['login']);
        $rules['created_at'] = 'required|positiveInt';
        return $rules;
    }

    protected function preInsertUser(array $data): array
    {
        $time = time();
        unset($data['id']);
        $data['created_at'] = $this->created_at = $time;
        $data['updated_at'] = $this->updated_at = $time;
        return $data;
    }

    protected function preUpdateUser(array $data): array
    {
        $time = time();
        unset($data['id']);
        $data['updated_at'] = $this->updated_at = $time;
        return $data;
    }

    protected function toArray(): array
    {
        $res = [];

        $fields = $this->listFiled();
        foreach($fields as $field) {
            $res[$field] = $this->$field;
        }

        $res['id'] = $this->id;

        return $res;
    }
}
