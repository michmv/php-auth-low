<?php

namespace MMV\Auth\Low;

use MMV\Auth\Low\StorageInterface;
use MMV\Auth\Low\Session\SecurityInterface;
use MMV\Auth\Low\Session\EnvironmentInterface;
use MMV\Auth\Low\Auth\SessionInterface;

class Session implements SessionInterface
{
    public string $table = 'sessions';

    protected $session = ['meta'=>[], 'data'=>[]];

    /**
     * Load data from storage
     */
    protected bool $loaded = false;

    /**
     * Session started
     */
    protected bool $started = false;

    /**
     * Session data was modify
     */
    protected bool $modified = false;

    /**
     * Type session
     * TRUE write only where data was modify or user joined to session
     * If FALSE to write where started
     */
    protected bool $lazy = true;

    protected SecurityInterface $security;

    protected StorageInterface $storage;

    protected EnvironmentInterface $environment;

    protected $logger = null;

    protected array $options = [
        'name' => 'PHPSESSION',
        'duration' => 36000, // 60 * 60 * 10
        'cookie' => ['path' => '/', 'domain' => null, 'secure' => null],
        'crypt_cookie' => true,
    ];

    protected $cache = [
        'id' => '',
        'dataString' => '',
        'user_id' => 0,
        'time_life' => 0,
        'cookie_value' => '',
    ];

    public function __construct(SecurityInterface $security, StorageInterface $storage,
        EnvironmentInterface $environment, array $options=[], $logger=null)
    {
        $this->security =$security;
        $this->storage = $storage;
        $this->environment = $environment;
        $this->options = array_merge($this->options, $options);
        $this->logger = $logger;
    }

    /**
     * @param string|int $name
     * @param mixed $default
     * @return mixed
     */
    public function get($name, $default=null)
    {
        $this->start();

        if(array_key_exists($name, $this->session['data'])) {
            $v = $this->session['data'][$name];
            if($v->flash) {
                $v->delete = true;
                $this->modified = true;
            }
            $res = $v->value;
        }
        else {
            $res = $default;
        }

        return $res;
    }

    public function has(string $name): bool
    {
        $this->start();

        return array_key_exists($name, $this->session['data']);
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param boolean $flash
     */
    public function set($name, $value, $flash=false)
    {
        $this->start();

        $t = new \stdClass;
        $t->value = $value;
        $t->flash = $flash;
        $t->delete = false;

        $this->session['data'][$name] = $t;

        $this->modified = true;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function flash($name, $value)
    {
        $this->set($name, $value, true);
    }

    public function all()
    {
        $this->start();

        $res = [];

        foreach($this->session['data'] as $key => $val) {
            if($val->flash) {
                $val->delete = true;
                $this->modified = true;
            }
            $res[$key] = $val->value;
        }

        return $res;
    }

    public function unset($name)
    {
        $this->start();

        if(array_key_exists($name, $this->session['data'])) {
            unset($this->session['data'][$name]);
            $this->modified = true;
        }
    }

    public function clear()
    {
        $this->start();

        $this->session['data'] = [];
        $this->modified = true;
    }

    public function start()
    {
        if($this->started) return;

        $this->readId();
        if($this->cache['id']) {
            $data = $this->storage->findRecord([
                ['id', '=', $this->cache['id']],
                ['time_life', '>', time()],
            ]);
            if($data) {
                // load session from storage
                $data = $data[0];
                $this->setSessionData($data);
                $this->loaded = true;
            }
            else {
                // not found session at storage
                $this->setSessionDefault();
            }
        }
        else {
            // new empty session
            $this->setSessionDefault();
        }

        $this->started = true;
    }

    public function save()
    {
        $this->start();

        if(($this->lazy && ($this->modified || $this->needSave())) || (!$this->lazy)) {

            // delete unset var
            foreach($this->session['data'] as $key => $val) {
                if($val->delete)
                    unset($this->session['data'][$key]);
            }

            if($this->cache['id']) {
                // update
                $this->cache['time_life'] = time() + $this->session['meta']['duration'];

                $modify = [
                    'user_id' => $this->cache['user_id'],
                    'time_life' => $this->cache['time_life'],
                ];

                if($this->modified) {
                    $this->cache['dataString'] = $modify['data'] = serialize($this->session);
                }

                $this->storage->updateRecord([ ['id', '=', $this->cache['id']] ], $modify);
            }
            else {
                // insert
                $this->cache['id'] = $this->security->uuid();
                $this->cache['cookie_value'] = ''; // reset cookie_value because new id
                $this->cache['dataString'] = serialize($this->session);
                $this->cache['time_life'] = time() + $this->session['meta']['duration'];

                $this->storage->insertRecord([
                    'id' => $this->cache['id'],
                    'data' => $this->cache['dataString'],
                    'user_id' => $this->cache['user_id'],
                    'time_life' => $this->cache['time_life'],
                ]);
            }

            $this->writeId();
        }
    }

    /**
     * @return string|int
     */
    public function getUserId()
    {
        $this->start();

        return $this->cache['user_id'];
    }

    /**
     * @param string $userId
     */
    public function setUserId($userId)
    {
        $this->start();

        $this->cache['user_id'] = $userId;
        
        $this->modified = true;
    }

    /**
     * @param int $duration
     */
    public function changeDuration($duration)
    {
        $this->start();

        $this->session['meta']['duration'] = $duration;

        $this->modified = true;
    }

    public function unlinkUserId()
    {
        $this->start();

        $this->cache['user_id'] = 0;
        $this->session['meta']['duration'] = $this->options['duration'];

        $this->modified = true;
    }

    public function removeAllSessionForUser($userId)
    {
        $this->storage->deleteRecord([['user_id', '=', $userId]]);
    }

    public function deleteSession()
    {
        if($this->cache['id']) {
            $this->storage->deleteRecord([['id', '=', $this->cache['id']]]);
        }

        $this->started = false;
        $this->loaded = false;
        $this->modified = false;

        $this->setSessionDefault();
        $this->setCacheDefault();
    }

    public function deleteOtherSessionOfUser()
    {
        if($this->cache['user_id']) {
            $this->storage->deleteRecord(
                [
                    ['user_id', '=', $this->cache['user_id']],
                    ['id', '<>', $this->cache['id']],
                ]
            );
        }
    }

    public function deleteRottenSession()
    {
        $time = time();
        $this->storage->deleteRecord([
            ['time_life', '<', $time],
        ]);
    }

    public function deleteAllSessionsOfAllUsers()
    {
        $this->storage->deleteRecord([]);
    }

    public function regenerate()
    {
        $this->start();

        if($this->cache['id']) {
            $this->deleteSession();
        }

        $this->cache['id'] = $this->cache['cookie_value'] = '';
        $this->modified = true;
    }

    protected function needSave(): bool
    {
        // user join to session, update session time_life
        return (bool)$this->cache['user_id'];
    }

    protected function readId()
    {
        $value = $this->environment->getCookie($this->options['name'], '');

        // write value before decrypt
        $this->cache['cookie_value'] = $value;

        // decrypt cookie
        if($value && $this->options['crypt_cookie']) {
            $value = $this->security->decrypt($value);

            if(!$value) $cache['cookie_value'] = '';
        }

        $this->cache['id'] = $value;
    }

    protected function writeId()
    {
        $config = $this->options['cookie'];

        if(!$this->cache['cookie_value']) {
            $this->cache['cookie_value'] = $this->options['crypt_cookie'] ?
                $this->security->encrypt($this->cache['id']) : // encrypt cookie
                $this->cache['id'];
        }

        $this->environment->setCookie($this->options['name'], $this->cache['cookie_value'], $this->cache['time_life'],
            $config['path'], $config['domain'], $config['secure'], true);
    }

    protected function setSessionDefault()
    {
        $this->cache['id'] = $this->cache['cookie_value'] = '';
        $this->session = [
            'meta' => ['duration' => $this->options['duration']],
            'data' => [],
        ];
    }

    protected function setCacheDefault()
    {
        $this->cache = [
            'id' => '',
            'dataString' => '',
            'user_id' => 0,
            'time_life' => 0,
            'cookie' => '',
        ];
    }

    protected function setSessionData($data)
    {
        $this->cache['dataString'] = $data->data;
        $this->cache['user_id'] = $data->user_id;
        $this->cache['time_life'] = $data->time_life;

        $data__ = unserialize($data->data);
        if(is_array($data__)) $this->session = $data__;
        else $this->setSessionDefault();
    }

    protected function log($message, array $context=[])
    {
    }
}
