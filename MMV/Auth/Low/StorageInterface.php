<?php

namespace MMV\Auth\Low;

interface StorageInterface
{
    public function setTable(string $name);

    public function getTable(): string;

    /**
     * @param array $data [key => value]
     * @param string|null $table
     * @return int
     */
    public function insertRecord($data, string $table=null): int;

    /**
     * @param array $conditions [[name, =, value]]
     * @param array $data [key => value]
     * @param string|null $table
     * @return int
     */
    public function updateRecord($conditions, $data, string $table=null): int;

    /**
     * @param array $conditions [[name, =, value]]
     * @param string|null $table
     * @return int
     */
    public function deleteRecord($conditions, string $table=null): int;

    /**
     * @param array $conditions [[name, =, value]]
     * @param string|null $table
     * @return int
     */
    public function countRecord($conditions, string $table=null): int;

    /**
     * @param array $conditions [[name, =, value]]
     * @param string|null $table
     * @return object[]|null [ [row from table] ]
     */
    public function findRecord($conditions, string $table=null): ?array;

    public function beginTransaction();

    public function commitTransaction();

    public function rollbackTransaction();
}