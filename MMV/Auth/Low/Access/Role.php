<?php

namespace MMV\Auth\Low\Access;

class Role
{
    public string $title = '';

    public string $description = '';

    public int $id = 0;

    public string $name = '';

    public array $allow = [];

    public array $deny = [];

    public array $dependence = [];
    
    /**
     * @var array
     */
    protected $cache = ['allow'=>null, 'deny'=>null];

    /**
     * @param int $id Must be unique value
     * @param string $name Must be unique value
     * @param array $allow
     * @param array $dependence
     * @param array $deny
     */
    public function __construct($id, $name, $allow=[], $dependence=[], $deny=[])
    {
        if(!$id)
            throw new \Exception('Id of role must be positive');

        $this->id = $id;
        $this->name = $name;
        $this->allow = $allow;
        $this->dependence = $dependence;
        $this->deny = $deny;
    }

    public function checkResource(string $nameResource): bool
    {
        $allow = $this->list('allow');
        $deny = $this->list('deny');
        return ( array_key_exists($nameResource, $allow) &&
                !array_key_exists($nameResource, $deny)
            );
    }

    public function title(): string
    {
        if($this->title) return $this->title;
        else return $this->name;
    }

    /**
     * @param string $type Value `allow` of `deny`
     * @return array
     */
    protected function list($type)
    {
        if($this->cache[$type] === null) {
            $res = [];
            foreach($this->dependence as $item) {
                $res = array_merge($res, $item->list($type));
            }
            $this->cache[$type] = array_merge($res, array_flip($this->$type));
        }
        return $this->cache[$type];
    }
}
