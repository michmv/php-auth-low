<?php

namespace MMV\Auth\Low\Access;

class Resource
{
    public string $name = '';

    public string $description = '';

    public string $group = '';

    /**
     * @param string $name
     * @param string $group
     * @param string $description
     */
    public function __construct($name, $description='', $group='')
    {
        if(!$name)
            throw new \Exception('Name can\'t be empty');

        $this->name = $name;
        $this->description = $description;
        $this->group = $group;
    }
}
