<?php

namespace MMV\Auth\Low\Validator;

interface ValidatorInterface
{
    /**
     * If return False to get messages `getMessages`.
     */
    public function validate(array $data, array $rules): bool;

    /**
     * @return array [fieldName => ['message']]
     */
    public function getMessages(): array;
}
