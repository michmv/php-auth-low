<?php

namespace MMV\Auth\Low\Auth;

interface SessionInterface
{
    /**
     * @return string|int
     */
    public function getUserId();

    /**
     * @param string|int $userId
     */
    public function setUserId($userId);

    public function changeDuration(int $duration);

    public function unlinkUserId();

    public function removeAllSessionForUser($userId);

    public function deleteOtherSessionOfUser();
}
