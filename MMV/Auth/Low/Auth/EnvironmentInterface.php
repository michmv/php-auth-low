<?php

namespace MMV\Auth\Low\Auth;

interface EnvironmentInterface
{
    public function getAgentId(): string;
}
