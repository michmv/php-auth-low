<?php

namespace MMV\Auth\Low\Auth;

interface SecurityInterface
{
    public function hash($value, array $options = []): string;

    public function check(string $value, string $hashedValue, array $options = []): bool;

    public function randomString(int $length): string;
}
