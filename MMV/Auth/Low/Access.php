<?php

namespace MMV\Auth\Low;

use MMV\Auth\Low\Access\Role;

class Access
{
    protected array $cache = ['number' => null, 'string' => null, 'resources' => null];

    protected array $roles = [];

    protected array $resources = [];

    /**
     * @param array $roles
     * @param array $resources
     */
    public function __construct($roles, $resources)
    {
        $this->roles = $roles;
        $this->resources = $resources;
    }

    /**
     * @param int|string $roleId
     * @param string $nameResource
     * @return bool
     */
    public function checkResource($roleId, string $nameResource): bool
    {
        if($role = $this->findRoleById($roleId)) {

            // resources must register in $this->resources
            if(!array_key_exists($nameResource, $this->cache['resources']))
                return false;

            return $role->checkResource($nameResource);
        }

        return false;
    }

    public function findRoleById($roleId): ?Role
    {
        $this->updateCache('number');
        return $this->cache['number'][$roleId] ?? null;
    }

    public function findRoleByName($roleName): ?Role
    {
        $this->updateCache('string');
        return $this->cache['string'][$roleName] ?? null;
    }

    /**
     * @return array
     */
    public function getResourcesGroup()
    {
        $res = [];

        // sort all resources by name
        $resources = $this->resources;
        usort($resources, function($a, $b) {
            if($a->name == $b->name) return 0;
            return ($a->name < $b->name) ? -1 : 1;
        });

        foreach($resources as $resource) {
            $group = (!$resource->group) ? '-' : $resource->group;
            $res[$group][] = $resource;
        }

        // sort groups
        ksort($res);

        return $res;
    }

    /**
     * @return \MMV\Auth\Low\Access\Role[]
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @return \MMV\Auth\Low\Access\Resource[]
     */
    public function getResources()
    {
        return $this->resources;
    }

    protected function updateCache($type)
    {
        if($type == 'number' && $this->cache['number'] === null) {
            $this->cache['number'] = [];
            foreach($this->roles as $it) {
                if(array_key_exists($it->id, $this->cache['number'])) {
                    throw new \RuntimeException('Id of role must be unique');
                }
                $this->cache['number'][$it->id] = $it;
            }
        }

        if($type == 'string' && $this->cache['string'] === null) {
            $this->cache['string'] = [];
            foreach($this->roles as $it) {
                if(array_key_exists($it->name, $this->cache['string'])) {
                    throw new \RuntimeException('Name of role must be unique');
                }
                $this->cache['string'][$it->name] = $it;
            }
        }

        if($this->cache['resources'] === null) {
            $this->cache['resources'] = [];
            foreach($this->resources as $it) {
                if(array_key_exists($it->name, $this->cache['resources'])) {
                    throw new \RuntimeException('Name for resources must be unique');
                }
                $this->cache['resources'][$it->name] = $it;
            }
        }
    }
}
