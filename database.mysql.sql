-- Replace prefix_

CREATE TABLE `prefix_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_confirmed` tinyint(1) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `updated_at` bigint(20) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `prefix_users_email_unique` (`email`),
  UNIQUE KEY `prefix_users_login_unique` (`login`),
  KEY `prefix_users_email_confirmed_index` (`email_confirmed`),
  KEY `prefix_users_created_at_index` (`created_at`),
  KEY `prefix_users_updated_at_index` (`updated_at`),
  KEY `role_id` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

CREATE TABLE `prefix_sessions` (
  `id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` blob NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `time_life` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `prefix_sessions_user_id_index` (`user_id`),
  KEY `prefix_sessions_time_life_index` (`time_life`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

CREATE TABLE `prefix_password_reset` (
  `record_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `code` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_life` bigint(20) NOT NULL,
  PRIMARY KEY (`record_id`),
  KEY `user_id` (`user_id`),
  KEY `limit` (`time_life`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

CREATE TABLE `prefix_email_confirm` (
  `record_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `code` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_life` bigint(20) NOT NULL,
  PRIMARY KEY (`record_id`),
  KEY `user_id` (`user_id`),
  KEY `limit` (`time_life`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

CREATE TABLE `prefix_failed_signin` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `agent` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `count` tinyint(4) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `prefix_failed_signin_user_id_index` (`user_id`),
  KEY `prefix_failed_signin_agent_index` (`agent`),
  KEY `prefix_failed_signin_created_at_index` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
