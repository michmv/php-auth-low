# List tables

### users
- id
- email
- password
- login
- email_confirmed
- created_at
- updated_at

### email_confirm
- record_id
- user_id
- code
- time_life

### password_reset
- record_id
- user_id
- code
- time_life

### failed_signin
- user_id
- agent
- count
- created_at

### List error messages

- email_or_password_incorrect
- user_not_found
- signin_was_blocked
- password_not_verification
- rules_not_found
- value_must_not_be_array
- value_is_required
- string_max|..
- string_min|..
- incorrect_email_format
- incorrect_alphanumeric
- double_separation_symbol
- incorrect_bool_int
- value_not_positive_int
- incorrect_date_format
- date_invalid
- not_unique_record
- not_unique_record_exclude_this
- not_confirm_password
- not_exists_in_array
