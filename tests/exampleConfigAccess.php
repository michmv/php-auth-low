<?php

use MMV\Auth\Low\Access\Resource;
use MMV\Auth\Low\Access\Role;

/**
 * Resources
 */

$resources = [
    new Resource('news.manage', 'Обновление блока новостей на сайте.'),
    new Resource('news.remove', 'Удалить новость с сайта.'),

    new Resource('chat.moderation', 'Модерирование чата. Может создавать комнаты, банить пользователей.'),
    new Resource('chat.room.remove', 'Удалить комнату чата.'),
];

/**
 * Newsmaker
 */
$newMaker = new Role(2, 'Newsmaker',
    [ 'news.manage'
    , 'news.remove'
    ]
);
$newMaker->title = 'Менеджер новостного отдела';

/**
 * Chat moder
 */
$chatModer = new Role(3, 'ChatModer',
    [ 'chat.moderation'
    , 'chat.room.remove'
    ]
);
$chatModer->title = 'Модератор чатов';

/**
 * Manager
 */
$manager = new Role(4, 'Manager',
    [],
    [$newMaker, $chatModer],
    [ 'news.remove'
    , 'chat.room.remove'
    ]
);
$manager->title = 'Менеджер сайта';
$manager->description = 'Разрешени все функции, кроме удаление новостей и комнат чата.';

/**
 * Admin
 */
$admin = new Role(1, 'Administrator', [],
    [$newMaker, $chatModer]
);
$admin->description = 'Имеет все права на сайте.';

/**
 * Return
 */

return [

    'roles' => [
        0 => $newMaker,
        1 => $chatModer,
        2 => $manager,
        3 => $admin,
    ],

    'resources' => $resources,
];
