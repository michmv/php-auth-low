<?php

use PHPUnit\Framework\TestCase;
use MMV\Auth\Low\Access\Resource;
use MMV\Auth\Low\Access;

class AccessTest extends TestCase
{
    /**
     * @return \MMV\PA\Utility\Access
     */
    public function getObject($config=[])
    {
        if(!$config) $config = include('exampleConfigAccess.php');
        return new Access($config['roles'], $config['resources']);
    }

    public function testUnknownRole()
    {
        $test = $this->getObject();

        $this->assertFalse($test->checkResource(10, 'news.manage'));
        $this->assertFalse($test->checkResource('unknown', 'news.manage'));
    }

    public function testUnknownResource()
    {
        $test = $this->getObject();

        $this->assertFalse($test->checkResource(1, 'unknown'));
        $this->assertFalse($test->checkResource('Administrator', 'unknown'));
    }

    public function testNotUniqueId()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('Id of role must be unique');

        $config = include('exampleConfigAccess.php');
        $config['roles'][0]->id = 10;
        $config['roles'][1]->id = 10;
        $test = $this->getObject($config);

        $test->checkResource(1, 'unknown');
    }

    public function testNotUniqueName()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('Name of role must be unique');

        $config = include('exampleConfigAccess.php');
        $config['roles'][0]->name = 'error';
        $config['roles'][1]->name = 'error';
        $test = $this->getObject($config);

        $test->checkResource('test', 'unknown');
    }

    public function testDependenceAllow()
    {
        $test = $this->getObject();

        $this->assertTrue($test->checkResource(4, 'chat.moderation'));
        $this->assertTrue($test->checkResource('Manager', 'chat.moderation'));
    }

    public function testDependenceAllowReload()
    {
        $config = include('exampleConfigAccess.php');
        $config['roles'][2]->deny = ['chat.moderation', 'chat.moderation'];
        $test = $this->getObject($config);

        $this->assertFalse($test->checkResource(4, 'chat.moderation'));
        $this->assertFalse($test->checkResource('Manager', 'chat.moderation'));
    }

    public function testDependenceDeny()
    {
        $config = include('exampleConfigAccess.php');
        $config['roles'][0]->deny = ['test.action'];
        $test = $this->getObject($config);

        $this->assertFalse($test->checkResource(1, 'test.action'));
        $this->assertFalse($test->checkResource('Administrator', 'test.action'));
    }

    public function testDependenceDenyReload()
    {
        $config = include('exampleConfigAccess.php');
        $config['roles'][0]->deny = ['test.action'];
        $config['roles'][3]->allow = ['test.action'];
        $test = $this->getObject($config);

        $this->assertFalse($test->checkResource(1, 'test.action'));
        $this->assertFalse($test->checkResource('Administrator', 'test.action'));
    }

    public function testDublicateResource()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('Name for resources must be unique');

        $config = include('exampleConfigAccess.php');
        $config['resources'][] = new Resource('news.manage', 'Bad resources');
        $test = $this->getObject($config);

        $test->checkResource('test', 'unknown');
    }

    public function testCheckNoRegisterResource()
    {
        $config = include('exampleConfigAccess.php');
        $config['resources'] = [];
        $test = $this->getObject($config);

        $this->assertFalse($test->checkResource(2, 'news.manage'));
    }
}
