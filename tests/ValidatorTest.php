<?php

use PHPUnit\Framework\TestCase;
use MMV\Auth\Low\StorageInterface;

class ValidatorTest extends TestCase
{
    public $database;

    public function getValidator()
    {
        $this->database = $this->createMock(StorageInterface::class);
        return new \MMV\Auth\Low\Validator($this->database);
    }

    /******************************************************************************/

    public function testDataWithoutRule()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'required'];
        $data = ['test' => '', 'bad'=>1];

        $result = $test->validate($data, $rules);

        $this->assertFalse($result);
        $this->assertEquals([
            'test' => ['value_is_required'],
            'bad' => ['rules_not_found']
        ], $test->getMessages());
    }

    public function testRequiredFail()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'required|lengthMax:5'];
        $data = [];

        $result = $test->validate($data, $rules);

        $this->assertFalse($result);
        $this->assertEquals(['test'=>['value_is_required']], $test->getMessages());
    }

    public function testRequiredOk()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'required'];
        $data = ['test' => 1];

        $result = $test->validate($data, $rules);

        $this->assertTrue($result);
    }

    public function testEmailFail()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'email'];

        $result = $test->validate(['test'=>'bad'], $rules);

        $this->assertFalse($result);
        $this->assertEquals(['test'=>['incorrect_email_format']], $test->getMessages());
    }

    public function testEmailOk()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'email'];

        $result = $test->validate(['test'=>'test@test.test'], $rules);

        $this->assertTrue($result);
    }

    public function testMinFail()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'lengthMin:3'];

        $result = $test->validate(['test'=>'1'], $rules);

        $this->assertFalse($result);
        $this->assertEquals(['test'=>['string_min|3']], $test->getMessages());
    }

    public function testMinOk()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'lengthMin:3'];

        $result = $test->validate(['test'=>'123'], $rules);

        $this->assertTrue($result);
    }

    public function testMaxFail()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'lengthMax:3'];

        $result = $test->validate(['test'=>'1234'], $rules);

        $this->assertFalse($result);
        $this->assertEquals(['test'=>['string_max|3']], $test->getMessages());
    }

    public function testMaxOk()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'lengthMax:3'];

        $result = $test->validate(['test'=>'123'], $rules);

        $this->assertTrue($result);
    }

    public function testAlphanumericFail()
    {
        $test = $this->getValidator();
        $rules = [
            'test' => 'alphanumeric',
            'test2' => 'alphanumeric',
        ];

        $result = $test->validate([
            'test'=>'a1:',
            'test2'=>'1a'
        ], $rules);

        $this->assertFalse($result);
        $this->assertEquals([
            'test'=>['incorrect_alphanumeric'],
            'test2'=>['incorrect_alphanumeric'],
        ], $test->getMessages());
    }

    public function testAlphanumericOk()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'alphanumeric'];

        $result = $test->validate(['test'=>'a.1-2_3'], $rules);

        $this->assertTrue($result);
    }

    public function testBool_IntFail()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'boolInt'];

        $result = $test->validate(['test'=>'4'], $rules);

        $this->assertFalse($result);
        $this->assertEquals(['test'=>['incorrect_bool_int']], $test->getMessages());
    }

    public function testBool_IntOk()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'boolInt', 'test2' => 'boolInt'];

        $result = $test->validate(['test'=>'1', 'test2'=>'0'], $rules);

        $this->assertTrue($result);
    }

    public function testPositive_IntFail()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'positiveInt', 'test2' => 'positiveInt'];

        $result = $test->validate(['test'=>'-1', 'test2'=>'a'], $rules);

        $this->assertFalse($result);
        $this->assertEquals([
            'test'=>['value_not_positive_int'],
            'test2'=>['value_not_positive_int'],
        ], $test->getMessages());
    }

    public function testPositive_IntOk()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'positiveInt'];

        $result = $test->validate(['test'=>'1'], $rules);

        $this->assertTrue($result);
    }

    public function testDateFail()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'date', 'test2' => 'date'];

        $result = $test->validate(
            ['test'=>'u', 'test2'=>'9999-99-99 99:99:99'], $rules);

        $this->assertFalse($result);
        $this->assertEquals([
            'test'=>['incorrect_date_format'],
            'test2'=>['date_invalid'],
        ], $test->getMessages());
    }

    public function testDateOk()
    {
        $test = $this->getValidator();
        $rules = ['test' => 'date'];

        $result = $test->validate(['test'=>'2009-10-10 20:30:40'], $rules);

        $this->assertTrue($result);
    }

    public function testUniqueFail()
    {
        $test = $this->getValidator();
        $this->database->expects($this->once())->method('countRecord')
            ->with(
                $this->equalTo([
                    ['name', '=', 'tester']
                ]),
                $this->equalTo('table')
            )
            ->will($this->returnValue(1));

        $rules = ['test' => 'unique:table:name'];
        $data = ['test' => 'tester'];

        $this->assertFalse($test->validate($data, $rules));
        $this->assertEquals(['test' => ['not_unique_record']], $test->getMessages());
    }

    public function testUniqueOk()
    {
        $test = $this->getValidator();
        $this->database->expects($this->once())->method('countRecord')
            ->will($this->returnValue(0));

        $rules = ['test' => 'unique:table:name'];
        $data = ['test' => 'tester'];

        $this->assertTrue($test->validate($data, $rules));
    }

    public function testUnique_Exclude_ThisFail()
    {
        $test = $this->getValidator();
        $this->database->expects($this->once())->method('countRecord')
            ->with(
                $this->equalTo([
                    ['name', '=', 'tester'],
                    ['id', '<>', 10],
                ]),
                $this->equalTo('table')
            )
            ->will($this->returnValue(1));

        $rules = ['test' => 'uniqueExcludeThis:id:table:name', 'id'=>'safe'];
        $data = ['test' => 'tester', 'id' => 10];

        $this->assertFalse($test->validate($data, $rules));
        $this->assertEquals(['test' => ['not_unique_record_exclude_this']], $test->getMessages());
    }

    public function testUnique_Exclude_ThisOk()
    {
        $test = $this->getValidator();
        $this->database->expects($this->once())->method('countRecord')
            ->will($this->returnValue(0));

        $rules = ['test' => 'uniqueExcludeThis:id:table:name', 'id'=>'safe'];
        $data = ['test' => 'tester', 'id' => 10];

        $this->assertTrue($test->validate($data, $rules));
    }

    public function testBail()
    {
        $test = $this->getValidator();

        $rules = ['test1' => 'lengthMin:5|bail|lengthMax:3', 'test2' => 'lengthMin:5|bail|lengthMax:3'];
        $data = ['test1' => '123', 'test2' => '12345'];

        $result = $test->validate($data, $rules);

        $this->assertFalse($result);
        $this->assertEquals([
            'test1' => ['string_min|5'],
            'test2' => ['string_max|3']
        ], $test->getMessages());
    }

    public function testValidArrya()
    {
        $test = $this->getValidator();

        $rules = ['test' =>
            'required|email|lengthMin:3|lengthMax:3|alphanumeric|boolInt|positiveInt'.
            '|date|unique:users:email|uniqueExcludeThis:id:users:email|confirmPassword:pass|inArray:0:1'
        ];
        $data = ['test' => ['value']];

        $result = $test->validate($data, $rules);

        $this->assertFalse($result);
        $this->assertEquals([
            'test' => ['value_must_not_be_array', 'value_must_not_be_array', 'value_must_not_be_array', 'value_must_not_be_array',
                'value_must_not_be_array', 'value_must_not_be_array', 'value_must_not_be_array', 'value_must_not_be_array',
                'value_must_not_be_array', 'value_must_not_be_array', 'value_must_not_be_array', 'value_must_not_be_array']
        ], $test->getMessages());
    }

    public function testConfirm_PasswordFail()
    {
        $test = $this->getValidator();

        $rules = ['pass' => 'safe', 'confirm' => 'confirmPassword:pass'];
        $data = ['pass'=>'123456', 'confirm'=>'qwerty'];

        $this->assertFalse($test->validate($data, $rules));
        $this->assertEquals(['confirm'=>['not_confirm_password']],
            $test->getMessages());
    }

    public function testConfirm_PasswordOk()
    {
        $test = $this->getValidator();

        $rules = ['pass' => 'safe', 'confirm' => 'confirmPassword:pass'];
        $data = ['pass'=>'123456', 'confirm'=>'123456'];

        $this->assertTrue($test->validate($data, $rules));
    }

    public function testInArrayFail()
    {
        $test = $this->getValidator();

        $rules = ['a' => 'inArray:0:1:2'];
        $data = ['a' => 10];

        $this->assertFalse($test->validate($data, $rules));
        $this->assertEquals(['a'=>['not_exists_in_array']], $test->getMessages());
    }

    public function testInArrayOk()
    {
        $test = $this->getValidator();

        $rules = ['a' => 'inArray:0:1:2'];
        $data = ['a' => 1];

        $this->assertTrue($test->validate($data, $rules));
    }

    public function testInArrayEmptyValueSet()
    {
        $test = $this->getValidator();

        $rules = ['a' => 'inArray:'];
        $data = ['a' => '1'];

        $this->assertFalse($test->validate($data, $rules));
    }

    public function testInArrayEmptyValueEmpty()
    {
        $test = $this->getValidator();

        $rules = ['a' => 'inArray:'];
        $data = ['a' => ''];

        $this->assertTrue($test->validate($data, $rules));
    }
}
